tp=(100,50,80)
#indexing
print(tp[1])
print(tp[-1])
#slicing
print(tp[:1])
#step-size
print(tp[::2])
#basic operation
inp=("keybord","scanner","mouse")
op=("monitor","speaker","printer")

print(inp+op)
print(inp*2)
print("keybord"in inp)
print("keybord"not in inp)
#access elements
games = ("vc","cc","coc","pubg","ff","cod","son")
for i in games:
    print(i)
for i in range(0,len(games)):
    print(i,"=",games[i])
for index,value in enumerate(games):
    print(index,':',value)
