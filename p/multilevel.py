class GrandParent:
    gm = "gully"
    def hello(self):
        print("it is a grand parent")
class Parent(GrandParent):
    game = "cricket"
    def info(self):
        print("it is a  parent")        
class child(Parent):
    gme = "pubg"
    def intro(self):
        super().info()
        super().hello()
        print("it is a child")        

obj = child()
print(obj.gm)
print(obj.game)
print(obj.gme)
obj.hello()
obj.info()
obj.intro()