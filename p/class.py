class Student:
    college = "sbbsu"
    is_book_issued = False
    def intro(self,name,age):
        self.n = name
        print("name: {} age: {} college: {}".format(name,age,self.college))
    def library(self,book_name):
        print("book: {} issued by: {}".format(book_name,self.n))    
st1 = Student()
st2 = Student()        
st1.intro("john",20)
st2.intro("petter",30)
st1.library("python programming")
print("BOOK ISSUED:" ,st1.is_book_issued)
print("BOOK ISSUED:" ,st2.is_book_issued)