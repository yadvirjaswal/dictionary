import random
options="""
    Press 1: To create account
    Press 2: To check balance
    Press 3: To deposit
    Press 4: To withdraw
    Press 5: Exit
"""
print(options)
users = []
def create_account():
    name= input("enter name: ")
    bal= int(input("enter initial amount: "))
    acc = random.randint(100,999)
    cust = {"name":name,"balance":bal,"account":acc}
    users.append(cust)
    print("Dear {} your acc created successfully , Acc no.{}".format(name,acc))
def check_bal():
    acc = int(input("enter account number: "))
    for i in users:
        if i['account'] == acc:
            print("Hello",i['name'],'!!!') 
            print("your balance , Rs: {}".format(i['balance']))   
def deposit():
    acc = int(input("enter account number: "))
    for i in users:
        if i['account'] == acc:
            amt=int(input("enter amount to deposit :"))
            i['balance']=i['balance']+amt
            print("hii {} acc.credited with Rs: {}".format(i['name'],amt))
            print("current balance Rs {}".format(i['balance']))
def withdraw():
    acc = int(input("enter account number: "))
    for i in users:
        if i['account'] == acc:
            amt=int(input("enter amount to withdraw :"))
            i['balance']=i['balance']-amt
            print("hii {} acc.credited with Rs: {}".format(i['name'],amt))
            print("current balance Rs {}".format(i['balance']))
while True:    
    ch = input("choose an action:")
    if ch=="1":
        create_account()
    elif ch=="2":
        check_bal() 
    elif ch =="3":
        deposit() 
    elif ch =="4":
        withdraw()         
    elif ch == "5":
        break
    else:
        print("invalid option")    