class base1:
    a = "base 1"
    def hello1(self):
        print("it is a function of base class")
class base2 :
    b = "base 2"
    def hello2(self):
         print("it is a function of base2")            
class main(base1,base2):
    c = "main class"
    def info(self):
         print("it is a function inside main class")
obj = main()

print(obj.c)
print(obj.a)
print(obj.b)
obj.info()
obj.hello1()
obj.info()