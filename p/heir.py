from datetime import datetime
class Student:
    def intro(self,name,clss,rno):
        print("Name: {}\nClass: {}\nRollno: {}\n".format(name,clss,rno))
        
class Account(Student):
    def fee_detail(self,total,pending):
        print("Total fee: {}/-\nPending fee: {}/-\n ".format(total,pending))

class Library(Student):
    def issue_book(self,bname,isd,rtd):
        print("Book name: {}\nIssue date: {}\nReturn date: {}\n".format(bname,isd,rtd))

acc=Account()
acc.intro("jack","python",1001)
acc.fee_detail(50000,5000)

lb = Library()
lb.intro("parker","django",1002)
lb.issue_book("Python Programming",datetime.now().date(),"01-Fed-2020")