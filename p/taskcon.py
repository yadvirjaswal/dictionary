import json
import requests 
data=requests.get('https://restcountries.eu/rest/v2/all')
l=data.json()
country=input('Enter country name : ').title()
flag=True
for x in l:
  if country == x['name'] :
    print('Name :',x['name'])
    print('Capital : ',x['capital'])
    print('Currency name : ',x['currencies'][0]['name'])
    border=''
    flag=False
    for z in x['borders']:
      border=border+(z+',')
    print('Borders : ',border.rstrip(','))
    print('Population : ',x['population'])
if(flag):
    print('No country exists for this name')
