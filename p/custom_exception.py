class MyError(Exception):
    def __init__(self,code):
        self.c = code
    def __str__(self):
        return str(self.c)

try:
    a,b = 10,20
    print(a+b)
    
    raise MyError(100001)

except MyError as val:
    print("Error code:{}".format(val))
    print("something went wrong!!")                