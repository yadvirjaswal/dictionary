#marks = [100,20,30,40,50]

# def check(v):
#     if v>=33:
#         return True

# result=list(filter(check,marks))
# print(result)
# print("total: ",len(result))        

# ls = ["aman","hello","petter","lorem"]
# def check(st):
#     return st.title()
# def perc(n):
#     p=(n/100)*100
#     return p    
# result = list(map(check,ls))
# print(result)  

# print(marks)
# percentage = list(map(perc,marks))
# print(percentage)
# print(list(map(lambda st:st.title(),ls)))

from functools import reduce

marks = [120,20,30,40,50,100,59,89,79]

result = reduce(lambda x,y: x+y,marks)

total = len(marks)*100
print((result/total)*100)

max = reduce(lambda a,b:a if a>b else b,marks)
print(max)